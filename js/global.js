//SOUNDS
var audio = [];

//TIMEOUTS
var timeout = [],
	timer;

//AWARDs NUMBER
var awardNum = 0;

function AudioPiece(source, start, end)
{
	this.audio = new Audio("audio/" + source + ".mp3");
	this.start = start;
	this.end = end;
	
	var currPiece = this,
		currAudio = this.audio;
	
	currAudio.addEventListener("timeupdate", function(){
		var currTime = Math.round(currAudio.currentTime);
		
		if(currTime == currPiece.end)
			currAudio.pause();
	});
}

AudioPiece.prototype.play = function()
{
	this.audio.currentTime = this.start;
	this.audio.play();
}

AudioPiece.prototype.pause = function()
{
	this.audio.pause();
}

AudioPiece.prototype.addEventListener = function(e, callBack)
{
	var currPiece = this,
		currAudio = this.audio;		
	
	if(e === "ended")
	{
		currAudio.addEventListener("timeupdate", function(){
			var currTime = Math.round(currAudio.currentTime);
			
			if(currTime === currPiece.end)
			{
				currAudio.pause();
				currAudio.currentTime += 3;
				callBack();
			}
		});
	}
	if(e === "timeupdate")
	{
		currPiece.audio.addEventListener("timeupdate", callBack);
	}
}

AudioPiece.prototype.getStart = function()
{
	return this.start;
}

Audio.prototype.update = function(callBack)
{
	this.addEventListener("timeupdate", callBack);
}

Audio.prototype.end = function(callBack)
{
	this.addEventListener("ended", callBack);
}

var startTimer = function(jqueryElement, secondsLeft, callBack)
{
	if(secondsLeft < 0)
		callBack();
	else
	{
		if(secondsLeft > 9)
			jqueryElement.html("00:"+secondsLeft--);
		else
			jqueryElement.html("00:0"+secondsLeft--);
		timer = setTimeout(function(){
			startTimer(jqueryElement, secondsLeft, callBack);
		}, 1000);
	}
}

var stopTimer = function()
{
	clearTimeout(timer);
}

var posAndSize = function(left, top, width, height)
{
	var pageW = 2000,
		pageH = 1000,
		leftP = left / 2000 * 100, 
		topP = top / 1000 * 100, 
		widthP = width / 2000 * 100,
		heightP = height / 1000 * 100;
		
	var css = {
		"left": leftP + "%",
		"top": topP + "%",
		"width": widthP + "%",
		"height": heightP + "%"
	};
	
	return css;
}
	
var allHaveHtml = function(jqueryElement){
	for(var i = 0; i < jqueryElement.length; i ++)
	{
		if(!$(jqueryElement[i]).html())
			return false;
	}
	return true;
}

var fadeOneByOne = function(jqueryElement, curr, interval, endFunction)
{
	if (curr < jqueryElement.length)
	{
		timeout[curr] = setTimeout(function(){
			$(jqueryElement[curr]).fadeIn(200);
			fadeOneByOne(jqueryElement, ++curr, interval, endFunction);
		}, interval);
	}
	else
		endFunction();
}

function TypingText(jqueryElement, interval, hasSound, endFunction)
{
	this.text = jqueryElement.html();
	this.jqueryElement = jqueryElement;
	this.interval = interval;
	this.currentSymbol = 0;
	this.endFunction = endFunction;
	this.hasSound = hasSound;
	this.audio = new Audio("audio/keyboard-sound.mp3");
	this.audio.addEventListener("ended", function(){
			this.play();
		});
	
	this.timeout;
	this.jqueryElement.html("");
}

TypingText.prototype.write = function()
{
	tObj = this;
	if (tObj.audio.paused && tObj.hasSound) {
		tObj.audio.play();
	}
	tObj.timeout = setTimeout(function(){
		if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 'n')
		{
			tObj.jqueryElement.append("<br>");
			tObj.currentSymbol += 2;
		}
		else if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 's')
		{
			tObj.currentSymbol += 2;
			tObj.jqueryElement.append("<sup>" + tObj.text[tObj.currentSymbol] + "</sup>");
			tObj.currentSymbol ++;
		}
		else if (tObj.text[tObj.currentSymbol] === "/" && tObj.text[tObj.currentSymbol + 1] === 'b')
		{
			tObj.currentSymbol += 2;
			tObj.jqueryElement.append("<b>" + tObj.text[tObj.currentSymbol] + "</b>");
			tObj.currentSymbol ++;
		}
		else
		{
			tObj.jqueryElement.append(tObj.text[tObj.currentSymbol]);
			tObj.currentSymbol ++;
		}
		if (tObj.currentSymbol < tObj.text.length) 
			tObj.write();
		else
		{
			tObj.audio.pause();
			tObj.endFunction();
		}
	}, tObj.interval);
}

TypingText.prototype.stopWriting = function()
{
	clearTimeout(this.timeout);
	this.audio.pause();
}

var loadImages = function(){
	jQuery.get('fileNames.txt', function(data) {
		var imagesSrc = data.split("\n"),
			images = [],
			loadPercentage = 0,
			imagesNum = imagesSrc.length - 1;
			unitToAdd = Math.round(100 / imagesNum);
		
		var imageLoadListener = function(){
			loadPercentage += unitToAdd;
			console.log(loadPercentage);
			imagesNum --;
			if (loadPercentage >= 100) {
				//hideEverythingBut($("#frame-000"));
			}
		};
		
		for (var i = 0; i < imagesSrc.length - 1; i ++)
		{
			images[i] = new Image();
			images[i].src = "pics/" + imagesSrc[i];
			images[i].addEventListener("load", imageLoadListener);
		}
	});
}

function DragTask(jqueryElements, successCondition, successFunction, failFunction, finishCondition, finishFunction)
{
	this.draggables = jqueryElements;
	this.draggabillies = [];
	this.vegetable;
	this.basket;
	
	this.makeThemDraggable = function()
	{
		for(var i = 0; i < this.draggables.length; i++)
			this.draggabillies[i] = new Draggabilly(this.draggables[i]);
	}
	
	this.addEventListeners = function()
	{
		for(var i = 0; i < this.draggabillies.length; i++)
		{
			this.draggabillies[i].on("dragStart", this.onStart);
			this.draggabillies[i].on("dragEnd", this.onEnd);
		}
	}
	
	this.onEnd = function(instance, event, pointer)
	{
		var currVeg = this.vegetable;
		var currBasket = this.basket;
		currVeg.fadeOut(0);
		currBasket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		currVeg.fadeIn(0);
		
		if (currBasket.attr("data-key") && successCondition(currVeg.attr("data-key"), currBasket.attr("data-key")))
				successFunction(currVeg, currBasket);
		else
			failFunction(currVeg, currBasket);
		
		currVeg.removeClass("box-shadow-white");
		currVeg.css("opacity", "");
		
		if (finishCondition())
		{
			finishFunction();
		}
	}
	
	this.onStart = function(instance, event, pointer)
	{
		this.vegetable = $(event.target);
		this.vegetable.css("z-index", "9999");
		this.vegetable.addClass("box-shadow-white");
		this.vegetable.css("opacity", "0.6");
	}
	
	this.makeThemDraggable();
	this.addEventListeners();
}

var blink = function(jqueryElements, color, interval, times, callBack)
{
	var intervalHalf = Math.round(interval/2);
	timeout[0] = setTimeout(function(){
		jqueryElements.css("background-color", color);
		timeout[1] = setTimeout(function(){
			jqueryElements.css("background-color", "");
			if (times) 
				blink(jqueryElements, color, interval, --times, callBack)		
			else
				callBack();
		}, intervalHalf);
	}, intervalHalf);
}

var launch000 = function()
{
	
}

var launch101 = function()
{
	var theFrame = $("#frame-101"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"), 
		bg1 = $(prefix + ".bg-1"),
		bg2 = $(prefix + ".bg-2"),
		bg3 = $(prefix + ".bg-3"),
		blackBG = $(prefix + ".black-bg"),
		law1 = $(prefix + ".law-1"),
		law2 = $(prefix + ".law-2");
	
	audio[0] = new Audio("audio/s1-1.mp3");
	
	audio[0].update(function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
			
		if(currTime === 15 && currTime !== doneSecond)
		{
			doneSecond = currTime;
			bg1.fadeOut(1000);
			bg2.fadeIn(1000);
		}
		else if(currTime === 25 && currTime !== doneSecond)
		{
			doneSecond = currTime;
			bg2.fadeOut(1000);
			bg3.fadeIn(1000);
		}
		else if(currTime === 32 && currTime !== doneSecond)
		{
			doneSecond = currTime;
			blackSkin.fadeIn(1000);
			law1.fadeIn(1000);
		}
		else if(currTime === 37 && currTime !== doneSecond)
		{
			doneSecond = currTime;
			law1.fadeOut(1000);
			law2.fadeIn(1000);
		}
	});
	
	audio[0].end(function(){
		law2.fadeOut(500);
		nextButton.fadeIn(1000);
	});
	
	nextButton.fadeOut(0);
	bg1.fadeOut(0);
	bg2.fadeOut(0);
	bg3.fadeOut(0);	
	law1.fadeOut(0);
	law2.fadeOut(0);
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		audio[0].play();
		timeout[0] = setTimeout(function(){
			bg1.fadeIn(1000);
		}, 1000);
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch102 = function()
{
	var theFrame = $("#frame-102"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		bg1 = $(prefix + ".bg-1"),
		bg2 = $(prefix + ".bg-2"),
		milkIceCream = $(prefix + ".milk-ice-cream"),
		boy1mouth = $(prefix + ".boy-1-mouth"),
		boy1mouth2 = $(prefix + ".boy-1-mouth-2"),
		boy2mouth = $(prefix + ".boy-2-mouth"),
		boy2mouth2 = $(prefix + ".boy-2-mouth-2"),
		shake = $(prefix + ".shake"),
		lomonosov = $(prefix + ".lomonosov"),
		boyJug = $(prefix + ".boy-jug"),
		clouds = $(prefix + ".cloud-kaz, " 
					+ prefix + ".cloud-rus, " 
					+ prefix + ".cloud-eng"),
		cloudFormula = $(prefix + ".cloud-formula"),
		boyAngry = $(prefix + ".boy-angry"), 
		video = $(prefix + ".video");
	
	audio[0] = new Audio("audio/s2-1.mp3");
		
	audio[0].update(function(){
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			currTimeFloat = currAudio.currentTime;
			doneSecond = 0;
			
		if(currTime === 5 && currTime !== doneSecond)
		{
			doneSecond = currTime;
			boy2mouth.fadeOut(0);
		
			timeout[1] = setTimeout(function(){
				bg1.fadeIn(500);
				milkIceCream.fadeIn(500);
			}, 1000);
			
			timeout[2] = setTimeout(function(){
				boy1mouth.fadeIn(0);
			}, 2000);
		}
		else if(currTime === 10 && currTimeFloat !== doneSecond)
		{
			doneSecond = currTimeFloat;
			blackSkin.fadeIn(500);
			shake.fadeIn(500);
		}
		else if(currTime === 13 && currTimeFloat != doneSecond)
		{
			doneSecond = currTimeFloat;
			boy1mouth.fadeOut(0);
			shake.fadeOut(500);
			boyJug.fadeIn(500);
			currAudio.pause();
			currAudio.currentTime = Math.round(currAudio.currentTime * 10) / 10 + 0.2;
			
			timeout[1] = setTimeout(function(){
				lomonosov.fadeIn(500);
				fadeOneByOne(clouds, 0, 500, function(){
					timeout[2] = setTimeout(function(){
						clouds.fadeOut(0);
						cloudFormula.fadeIn(0);
					}, 2000);
					timeout[3] = setTimeout(function(){
						boyJug.fadeOut(0);
						lomonosov.fadeOut(0);
						blackSkin.fadeOut(0);
						bg1.fadeOut(0);
						bg2.fadeIn(0);
						cloudFormula.fadeOut(0);
						milkIceCream.fadeOut(0);
						boy2mouth2.fadeIn(0);
						currAudio.play();
					}, 5000);
				});
			}, 2000);
		}
		else if(currTime === 15 && currTimeFloat !== doneSecond)
		{
			doneSecond = currTimeFloat;
			boy2mouth2.fadeOut(0);
			blackSkin.fadeIn(0);
			boyAngry.fadeIn(0);
			timeout[4] = setTimeout(function(){
				boy1mouth2.fadeIn(0);
			}, 1000);
			timeout[4] = setTimeout(function(){
				boy1mouth2.fadeOut(0);
			}, 4000);
		}
		else if(currTime === 22 && currTimeFloat !== doneSecond)
		{
			doneSecond = currTimeFloat;
			boyAngry.fadeOut(0);
			timeout[5] = setTimeout(function(){
				video.parent().fadeIn(0);
				video.attr("width", video.parent().css("width"));
				video.attr("height", video.parent().css("height"));
				video[0].play();
			}, 4000);
		}
	});

	video[0].addEventListener("ended", function(){
		video.parent().fadeOut(0);
		blackSkin.fadeIn(500);
		nextButton.fadeIn(500);
	});
	
	boyAngry.fadeOut(0);
	nextButton.fadeOut(0);
	boy2mouth2.fadeOut(0);
	boy1mouth.fadeOut(0);
	boy2mouth.fadeOut(0);
	boy1mouth2.fadeOut(0);
	bg1.fadeOut(0);
	bg2.fadeOut(0);
	milkIceCream.fadeOut(0);
	shake.fadeOut(0);
	clouds.fadeOut(0);
	lomonosov.fadeOut(0);
	boyJug.fadeOut(0);
	cloudFormula.fadeOut(0);
	video.parent().fadeOut(0);
	
	video.attr("width", video.parent().css("width"));
	video.attr("height", video.parent().css("height"));
	
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		timeout[0] = setTimeout(function(){
			boy2mouth.fadeIn(0);
			audio[0].play();
		}, 1000);
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch103 = function()
{
	var theFrame = $("#frame-103"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		tvFrame = $(prefix + ".tv-frame"),
		boys1 = $(prefix + ".boys-1"),
		boys2 = $(prefix + ".boys-2"),
		tvProgram = $(prefix + ".tv-program");
		
	audio[0] = new AudioPiece("s3-1", 0, 17);
	
	audio[0].addEventListener("ended", function(){
		timeout[1] = setTimeout(function(){
			boys1.fadeOut(0);
			boys2.fadeIn(0);
		}, 1000);
		timeout[2] = setTimeout(function(){
			boys2.fadeOut(0);
			tvProgram.fadeOut(0);
			tvFrame.fadeIn(500);
			nextButton.fadeIn(0);
		}, 3000);
	});
	
	nextButton.fadeOut(0);
	boys2.fadeOut(0);
	tvFrame.fadeOut(0);
	tvProgram.fadeOut(0);
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		timeout[0] = setTimeout(function(){
			audio[0].play();
			tvProgram.fadeIn(500);
		}, 1000);
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch104 = function()
{
	var theFrame = $("#frame-104"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		label1 = $(prefix + ".label-1"),
		bingo1 = $(prefix + ".bingo-1"), 
		bingo2 = $(prefix + ".bingo-2"),
		datas = $(prefix + ".data"),
		timer = $(prefix + ".timer"),
		helpButton = $(prefix + ".help-button"),
		hintPic = $(prefix + ".hint-pic"),
		strawberryContainer = $(prefix + ".strawberry-container"),
		strawberry = "<div class='strawberry'></div>",
		correctAnswer = ["h2o", "o2"],
		activeAudio = 0;
	
	audio[0] = new AudioPiece("s3-1", 21, 42);
	audio[1] = new AudioPiece("s3-1", 45, 55);
	audio[2] = new AudioPiece("s3-1", 55, 66);
	
	var goFurther = function(){
		var el = $(prefix + ".data.trueanswer");
		blink(el, "blue", 1000, 3, function(){
			helpButton.fadeOut(0);
			datas.fadeOut(500);
			if(activeAudio < 3)
			{
				audio[++activeAudio].play();
				console.log("go-further: " + activeAudio);
			}
			else
			{
				blackSkin.fadeIn(0);
				nextButton.fadeIn(0);
			}			
			el.removeClass("trueanswer");
			if(correctAnswer.length)
				correctAnswer.shift();
			$(prefix + "." + correctAnswer[0]).addClass("trueanswer");
			hintPic.css("background-image","url(pics/hint-" + correctAnswer[0] + ".png)");
		});
	};
	
	audio[activeAudio].addEventListener("ended", function(){ 
		timeout[0] = setTimeout(function(){
			audio[++activeAudio].play();
			console.log("eventlistener: " + activeAudio);
		}, 2000);
	});
	audio[1].addEventListener("ended", function(){ 
		helpButton.fadeIn(0);
		datas.fadeIn(500);
		startTimer(timer, 7, function(){
			goFurther();
		});
	});
	audio[2].addEventListener("ended", function(){ 
		helpButton.fadeIn(0);
		datas.fadeIn(500);
		startTimer(timer, 7, function(){
			goFurther();
		});
	});
	
	labelType = new TypingText(label1, 100, false, function(){
		bingo1.css(posAndSize(68, 251, 1289, 653));
		label1.css(posAndSize(200, 83, 1600, 155));
	});
	
	var dataListener = function(){
		stopTimer();
		var currElem = $(this);
		if(currElem.hasClass("trueanswer"))
			strawberryContainer.append(strawberry);
		else
		{
			currElem.css("background-color", "red");
			timeout[5] = setTimeout(function(){
				currElem.css("background-color", "");
			}, 2000);
		}
		goFurther();			
	};
	datas.off("click", dataListener);
	datas.on("click", dataListener);
	
	var helpListener = function(){
		hintPic.fadeIn(500);
		timeout[0] = setTimeout(function(){
			hintPic.fadeOut(500);
		}, 2000);
	}
	helpButton.off("click", helpListener);
	helpButton.on("click", helpListener);
	
	nextButton.fadeOut(0);
	bingo2.fadeOut(0);
	datas.fadeOut(0);
	helpButton.fadeOut(0);
	hintPic.fadeOut(0);
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		labelType.write();
		audio[0].play();
	};
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch105 = function()
{
	var theFrame = $("#frame-105"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		label1 = $(prefix + ".label-1"),
		bingo1 = $(prefix + ".bingo-1"), 
		bingo2 = $(prefix + ".bingo-2"),
		datas = $(prefix + ".data"),
		timer = $(prefix + ".timer"),
		helpButton = $(prefix + ".help-button"),
		hintPic = $(prefix + ".hint-pic"),
		taskPic = $(prefix + ".task-pic"),
		bananaContainer = $(prefix + ".banana-container"),
		banana = "<div class='banana'></div>",
		taskPics = ["task-sulfuric-acid", "task-calcium-oxide"],
		correctAnswer = ["g98", "g56"],
		activeAudio = 0;
	
	audio[0] = new Audio("audio/s4-1.mp3");
	
	var goFurther = function(){
		var el = $(prefix + ".data.trueanswer");
		blink(el, "blue", 1000, 3, function(){
			taskPics.shift();
			if(taskPics.length)
			{
				taskPic.css("background-image", "url(pics/"+taskPics[0]+".png)");	
				startTimer(timer, 7, function(){
					goFurther();
				});				
			}
			else
			{
				blackSkin.fadeIn(0);
				nextButton.fadeIn(0);
			}			
			el.removeClass("trueanswer");
			
			correctAnswer.shift();
			$(prefix + "." + correctAnswer[0]).addClass("trueanswer");
		});
	};
	
	audio[0].addEventListener("ended", function(){ 
		helpButton.fadeIn(0);
		datas.fadeIn(500);
		taskPic.fadeIn(500);
		startTimer(timer, 7, function(){
			goFurther();
		});
	});
	
	labelType = new TypingText(label1, 100, false, function(){
		bingo1.css(posAndSize(68, 251, 1289, 653));
		label1.css(posAndSize(200, 83, 1600, 155));
	});
	
	var dataListener = function(){
		stopTimer();
		var currElem = $(this);
		if(currElem.hasClass("trueanswer"))
			bananaContainer.append(banana);
		else
		{
			currElem.css("background-color", "red");
			timeout[5] = setTimeout(function(){
				currElem.css("background-color", "");
			}, 2000);
		}
		goFurther();			
	};
	datas.off("click", dataListener);
	datas.on("click", dataListener);
	
	var helpListener = function(){
		hintPic.fadeIn(500);
	}
	helpButton.off("click", helpListener);
	helpButton.on("click", helpListener);
	
	var hintPicListener = function(){
		$(this).fadeOut(0);
	}
	hintPic.off("click", hintPicListener);
	hintPic.on("click", hintPicListener);
	
	nextButton.fadeOut(0);
	bingo2.fadeOut(0);
	datas.fadeOut(0);
	helpButton.fadeOut(0);
	hintPic.fadeOut(0);
	taskPic.fadeOut(0);
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		labelType.write();
		audio[0].play();
	};
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch106 = function()
{
	var theFrame = $("#frame-106"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		boy1mouth = $(prefix + ".boy-1-mouth"),
		boy2mouth = $(prefix + ".boy-2-mouth");
		
	audio[0] = new Audio("audio/s5-1.mp3");
	
	audio[0].addEventListener("timeupdate", function(){ 
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
			
		if(currTime === 5 && currTime != doneSecond)
		{
			boy2mouth.fadeOut(0); 
			boy1mouth.fadeIn(0); 
		}
		if(currTime === 23 && currTime != doneSecond)
		{
			boy1mouth.fadeOut(0); 
			blackSkin.fadeIn(0); 
			nextButton.fadeIn(0);
			currAudio.pause();
		}
	});
	
	nextButton.fadeOut(0);
	boy1mouth.fadeOut(0);
	boy2mouth.fadeOut(0);
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		timeout[0] = setTimeout(function(){
			audio[0].play();
			boy2mouth.fadeIn(0);
		}, 1000);
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch107 = function()
{
	var theFrame = $("#frame-107"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin");
		
	audio[0] = new AudioPiece("s5-1", 22, 31);
		
	nextButton.fadeOut(0);
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		timeout[0] = setTimeout(function(){
			audio[0].play();
		}, 1000);
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch107a = function()
{
	var theFrame = $("#frame-107-a"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		balls = $(prefix + ".ball"),
		baskets = $(prefix + ".basket");
	
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	}
	
	var successFunction = function(vegetable, basket){
		basket.css("background-image", vegetable.css("background-image"));
		vegetable.css("top", "");
		vegetable.css("left", "");
	}
	
	var failFunction = function(vegetable, basket){
		vegetable.css("top", "");
		vegetable.css("left", "");
	}
	
	var finishCondition = function(){
		for (var i = 0; i < baskets.length; i ++)
			if($(baskets[i]).css("background-image") === "none")
				return false;
		return true;
	}
	
	var finishFunction = function(){
		nextButton.fadeIn(0);
		blackSkin.fadeIn(0);
	}
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	audio[0] = new AudioPiece("s5-1", 35, 999);
		
	nextButton.fadeOut(0);
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		timeout[0] = setTimeout(function(){
			audio[0].play();
		}, 1000);
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch107b = function()
{
	var theFrame = $("#frame-107-b"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		balls = $(prefix + ".ball"),
		baskets = $(prefix + ".basket");
	
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	}
	
	var successFunction = function(vegetable, basket){
		basket.css("background-image", vegetable.css("background-image"));
		vegetable.css("top", "");
		vegetable.css("left", "");
	}
	
	var failFunction = function(vegetable, basket){
		vegetable.css("top", "");
		vegetable.css("left", "");
	}
	
	var finishCondition = function(){
		for (var i = 0; i < baskets.length; i ++)
			if($(baskets[i]).css("background-image") === "none")
				return false;
		return true;
	}
	
	var finishFunction = function(){
		nextButton.fadeIn(0);
		blackSkin.fadeIn(0);
	}
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	audio[0] = new AudioPiece("s5-1", 35, 999);
		
	nextButton.fadeOut(0);
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		timeout[0] = setTimeout(function(){
			audio[0].play();
		}, 1000);
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch107c = function()
{
	var theFrame = $("#frame-107-c"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		balls = $(prefix + ".ball"),
		baskets = $(prefix + ".basket");
	
	var successCondition = function(vegetable, basket){
		return vegetable === basket;
	}
	
	var successFunction = function(vegetable, basket){
		basket.css("background-image", vegetable.css("background-image"));
		vegetable.css("top", "");
		vegetable.css("left", "");
	}
	
	var failFunction = function(vegetable, basket){
		vegetable.css("top", "");
		vegetable.css("left", "");
	}
	
	var finishCondition = function(){
		for (var i = 0; i < baskets.length; i ++)
			if($(baskets[i]).css("background-image") === "none")
				return false;
		return true;
	}
	
	var finishFunction = function(){
		nextButton.fadeIn(0);
		blackSkin.fadeIn(0);
	}
	
	var dragTask = new DragTask(balls, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	audio[0] = new AudioPiece("s5-1", 35, 999);
		
	nextButton.fadeOut(0);
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		timeout[0] = setTimeout(function(){
			audio[0].play();
		}, 1000);
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch108 = function()
{
	var theFrame = $("#frame-108"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		matches = $(prefix + ".matches"),
		equation = $(prefix + ".equation"),
		textfields = $(prefix + ".textfield"),
		title = $(prefix + ".title"),
		checkButton = $(prefix + ".check-button"), 
		pearContainer = $(prefix + ".pear-container"),
		pear = "<div class='pear'></div>";
	
	audio[0] = new Audio("audio/s6-1.mp3");
	
	audio[0].addEventListener("timeupdate", function(){ 
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
			
		if(currTime === 10 && currTime !== doneSecond)
		{
			doneSecond = currTime;
			timeout[0] = setTimeout(function(){
				matches.removeClass("pos-0");
				matches.addClass("pos-1");
			}, 1000);
		}
		if(currTime === 35 && currTime !== doneSecond)
		{
			doneSecond = currTime;
				timeout[1] = setTimeout(function(){
				matches.removeClass("pos-1");
				matches.addClass("pos-2");
			}, 2000);
			
			timeout[2] = setTimeout(function(){
				equation.fadeIn(500);
				textfields.fadeIn(500);
				title.fadeIn(500);
			}, 4000);
		}
	});
	
	textfields.val("");	
	nextButton.fadeOut(0);
	equation.fadeOut(0);
	textfields.fadeOut(0);
	title.fadeOut(0);
	checkButton.fadeOut(0);
	
	var keyUpListener = function(){
		var full = true;
		for(var i = 0; i < textfields.length; i++)
			if(!$(textfields[i]).val())
				full = false;
		if(full)
			checkButton.fadeIn(500);
	}
	textfields.off("keyup", keyUpListener);
	textfields.on("keyup", keyUpListener);
	
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		timeout[0] = setTimeout(function(){
			audio[0].play();
		}, 1000);
	};
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
	
	var checkButtonListener = function(){
		for (var i = 0; i < textfields.length; i++)
		{
			if($(textfields[i]).val() === $(textfields[i]).attr("data-correct"))
			{
				pearContainer.append(pear);
				$(textfields[i]).css("background-color", "blue")
				$(textfields[i]).css("color", "white")
			}
			else
			{
				$(textfields[i]).css("background-color", "red")
				$(textfields[i]).css("color", "white")
			}			
		}
		
		timeout[2] = setTimeout(function(){
			blackSkin.fadeIn(0);
			nextButton.fadeIn(0);
		}, 3000);
	}
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
}

var launch109 = function()
{
	var theFrame = $("#frame-109"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		boyMouth = $(prefix + ".boy-mouth"),
		pancakes = $(prefix + ".pancakes"),
		cloud = $(prefix + ".cloud"),
		vinegarAndSoda = $(prefix + ".vinegar, " + prefix + ".soda");
	
	audio[0] = new AudioPiece("s7-1", 0, 12);
	
	audio[0].addEventListener("ended", function(){ 
		boyMouth.fadeOut(0);
		timeout[1] = setTimeout(function(){
			nextButton.fadeIn(0);
			blackSkin.fadeIn(0);
		}, 2000);
	});
	
	nextButton.fadeOut(0);
	cloud.fadeOut(0);
	vinegarAndSoda.fadeOut(0);
	pancakes.fadeOut(0);
	boyMouth.fadeOut(0);
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		timeout[0] = setTimeout(function(){
			audio[0].play();
			boyMouth.fadeIn(0);
			timeout[1] = setTimeout(function(){
				cloud.fadeIn(500);
				vinegarAndSoda.fadeIn(1000);
				pancakes.fadeIn(500);
			}, 1000);
		}, 1000);
	};
	
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch110 = function()
{
	var theFrame = $("#frame-110"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		avogadroLaw = $(prefix + ".avogadro-law"),
		formula = $(prefix + ".formula"),
		law = $(prefix + ".law"),
		hintPic = $(prefix + ".hint-pic"),
		closeButton = $(prefix + ".hint-pic .close-button"),
		law1Link = $(prefix + ".law-1-link"),
		law2Link = $(prefix + ".law-2-link"),
		law3Link = $(prefix + ".law-3-link"), 
		task = $(prefix + ".task"),
		textfield = $(prefix + ".textfield"),
		checkButton = $(prefix + ".check-button");
	
	audio[0] = new AudioPiece("s7-1", 12, 999);
	
	audio[0].addEventListener("timeupdate", function(){ 
		var currAudio = this,
			currTime = Math.round(currAudio.currentTime),
			doneSecond = 0;
		
		if(currTime === 31 && currTime !== doneSecond)
		{
			currAudio.pause();
			formula.fadeOut(0);
			task.fadeIn(0);
			hintPic.fadeOut(0);
			checkButton.fadeOut(0);
		}
	});	
	
	textfield.val("");
	nextButton.fadeOut(0);
	avogadroLaw.fadeOut(0);
	formula.fadeOut(0);
	task.fadeOut(0);

	var checkButtonListener = function(){
		if(textfield.val() === textfield.attr("data-correct"))
		{
			textfield.css("background-color", "blue");
			timeout[5] = setTimeout(function(){
				hintPic.css("background-image", "url(pics/pine-apple.png)");
				blackSkin.fadeIn(0);
				hintPic.fadeIn(500);
				timeout[6] = setTimeout(function(){
					hintPic.fadeOut(0);
					nextButton.fadeIn(0);
				}, 2000);
			}, 3000);
		}
		else
			textfield.css("background-color", "red");
	}
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	var textfieldListener = function(){
		if(textfield.val() !== "")
			checkButton.fadeIn(500);
	};
	textfield.off("keyup", textfieldListener);
	textfield.on("keyup", textfieldListener);
	
	var law1Listener = function(){
		hintPic.css("background-image", "url(pics/hint-avogadro-law.png)");
		hintPic.fadeIn(0);
		blackSkin.fadeIn(0);
	}
	law1Link.off("click", law1Listener);
	law1Link.on("click", law1Listener);
	
	var law2Listener = function(){
		hintPic.css("background-image", "url(pics/hint-mass-keep.png)");
		hintPic.fadeIn(0);
		blackSkin.fadeIn(0);
	}
	law2Link.off("click", law2Listener);
	law2Link.on("click", law2Listener);
	
	var law3Listener = function(){
		hintPic.css("background-image", "url(pics/hint-formula.png)");
		hintPic.fadeIn(0);
		blackSkin.fadeIn(0);
	}
	law3Link.off("click", law3Listener);
	law3Link.on("click", law3Listener);
	
	var closeButtonListener = function(){
		hintPic.fadeOut(0);
		blackSkin.fadeOut(0);
	}
	closeButton.off("click", closeButtonListener);
	closeButton.on("click", closeButtonListener);
	
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		timeout[0] = setTimeout(function(){
			audio[0].play();
			timeout[1] = setTimeout(function(){
				fadeOneByOne(avogadroLaw, 0, 1000, function(){
					timeout[2] = setTimeout(function(){
						avogadroLaw.fadeOut(0);
						formula.fadeIn(500);
					}, 3000);
				});
			}, 1000);
		}, 1000);
	};
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch111 = function()
{
	var theFrame = $("#frame-111"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		fruits = $(prefix + ".fruits");
	
	audio[0] = new Audio("audio/s8-1.mp3");
	audio[0].addEventListener("ended", function(){
		blackSkin.fadeIn(0);
		nextButton.fadeIn(0);
	});
	
	nextButton.fadeOut(0);
	fruits.fadeOut(0);
	
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		audio[0].play();
		fruits.fadeIn(500);
		fruits.addClass("transformed");
	};
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch201 = function()
{
	var theFrame = $("#frame-201"),
		prefix = "#" + theFrame.attr("id") + " ",
		date = $(prefix + ".date");
		
	var now = new Date(),
		day, month, year, hours, minutes;
	
	now.getDay() < 10 ? day = "0" + now.getDay() : day = now.getDay();
	now.getMonth() < 10 ? month = "0" + now.getMonth() : month = now.getMonth();
	year = now.getYear();
	hours = now.getHours();
	minutes = now.getMinutes();
	
	date.append(": <u>" + day + "." + month + "." + year + " / " + hours + ":" + minutes + "</u>");
	window.print();
	hideEverythingBut($("#frame-000"));
}
 
var launch401 = function()
{
	var theFrame = $("#frame-401"),
		prefix = "#" + theFrame.attr("id") + " ",
		startButton = $(prefix + ".start-button"),
		nextButton = $(prefix + ".next-button"),
		blackSkin = $(prefix + ".black-skin"),
		text = $(prefix + ".text"),
		scales = $(prefix + ".scales"),
		scalesEmpty = $(prefix + ".scales-empty"),
		scalesBefore = $(prefix + ".scales-before"),
		scalesAfter = $(prefix + ".scales-after"),
		burner = $(prefix + ".burner"),
		shtativ = $(prefix + ".shtativ"),
		spoon = $(prefix + ".spoon"),
		petryEmpty = $(prefix + ".petry-empty"),
		petryFull = $(prefix + ".petry-full"),
		tigelClosed = $(prefix + ".tigel-closed"),
		tigelEmpty = $(prefix + ".tigel-empty"),
		tigelFull = $(prefix + ".tigel-full"),
		comments = $(prefix + ".comment"), 
		comment1 = $(prefix + ".comment-1"),
		comment2 = $(prefix + ".comment-2"),
		comment3 = $(prefix + ".comment-3"),
		info = $(prefix + ".info"),
		info1 = $(prefix + ".info-1"),
		info2 = $(prefix + ".info-2"), 
		info3 = $(prefix + ".info-3"),
		printButton = $(prefix + ".print-button");
	
	var printButtonListener = function(){
		hideEverythingBut($("#frame-402"));
	}
	printButton.off("click", printButtonListener);
	printButton.on("click", printButtonListener);
	
	var spoonSprite = new Motio(spoon[0], {
		"frames": "3",
		"fps": "2"
	});
	
	var textType = new TypingText(text, 50, true, function(){
		text.fadeOut(500);
		timeout[1] = setTimeout(function(){
			spoon.fadeOut(500);
			petryEmpty.fadeOut(500);
			petryFull.fadeOut(500);
		}, 2000);
		timeout[2] = setTimeout(function(){
			tigelClosed.addClass("tigel-on-scale");
		}, 2000);
		timeout[3] = setTimeout(function(){
			scalesEmpty.fadeIn(0);
			comment1.fadeIn(500);
		}, 4000);
		timeout[4] = setTimeout(function(){
			scalesEmpty.fadeOut(0);
			comment1.fadeOut(0);
			tigelClosed.removeClass("tigel-on-scale");
		}, 6000);
		timeout[5] = setTimeout(function(){
			tigelClosed.fadeOut(0);
			tigelEmpty.fadeIn(0);
			petryFull.fadeIn(0);
			spoon.fadeIn(0);
		}, 9000);
		timeout[6] = setTimeout(function(){
			spoon.addClass("transition-2s");
			spoon.addClass("spoon-petry");
		}, 12000);
		timeout[7] = setTimeout(function(){
			spoon.removeClass("transition-2s");
			spoonSprite.to(2, true);
			petryFull.fadeOut(0);
			petryEmpty.fadeIn(0);
		}, 14000);
		timeout[8] = setTimeout(function(){
			spoon.addClass("transition-2s");
			spoon.addClass("spoon-tigel");
		}, 16000);
		timeout[9] = setTimeout(function(){
			spoon.removeClass("transition-2s");
			spoonSprite.to(0, true);
			tigelEmpty.fadeOut(0);
			tigelFull.fadeIn(0);
		}, 18000);
		timeout[10] = setTimeout(function(){
			spoon.fadeOut(500);
			tigelEmpty.fadeOut(0);
			tigelFull.fadeIn(0);
		}, 20000);
		timeout[11] = setTimeout(function(){
			tigelFull.addClass("tigel-on-scale");
		}, 22000);
		timeout[12] = setTimeout(function(){
			scalesBefore.fadeIn(0);
			comment2.fadeIn(0);
		}, 24000);
		timeout[13] = setTimeout(function(){
			scalesBefore.fadeOut(0);
			comment2.fadeOut(0);
			tigelFull.removeClass("tigel-on-scale");
		}, 26000);
		timeout[14] = setTimeout(function(){
			tigelFull.fadeOut(0);
			tigelClosed.fadeIn(0);
		}, 28000);
		timeout[15] = setTimeout(function(){
			burner.fadeIn(0);
		}, 30000);
		timeout[16] = setTimeout(function(){
			burner.fadeOut(0);
		}, 33000);
		timeout[17] = setTimeout(function(){
			tigelClosed.addClass("tigel-on-scale");
		}, 35000);
		timeout[18] = setTimeout(function(){
			comment3.fadeIn(0);
			scalesAfter.fadeIn(0);
		}, 37000);
		timeout[19] = setTimeout(function(){
			info1.fadeIn(0);
		}, 40000);
		timeout[20] = setTimeout(function(){
			info1.fadeOut(0);
			info2.fadeIn(0);
		}, 45000);
		timeout[21] = setTimeout(function(){
			info2.fadeOut(0);
			info3.fadeIn(0);
			printButton.fadeIn(0);
		}, 55000);
	});
	
	audio[0] = new Audio("audio/s10_1.mp3");
	audio[0].addEventListener("ended", function(){
		blackSkin.fadeIn(0);
		nextButton.fadeIn(0);
	});
	
	nextButton.fadeOut(0);
	text.fadeOut(0);
	scalesBefore.fadeOut(0);
	scalesAfter.fadeOut(0);
	scalesEmpty.fadeOut(0);
	tigelEmpty.fadeOut(0);
	tigelFull.fadeOut(0);
	burner.fadeOut(0);
	comments.fadeOut(0);
	info.fadeOut(0);
	printButton.fadeOut(0);
		
	var startButtonListener = function(){
		startButton.fadeOut(0);
		blackSkin.fadeOut(0);
		text.fadeIn(500);
		textType.write();
	};
	startButton.off("click", startButtonListener);
	startButton.on("click", startButtonListener);
}

var launch402 = function()
{
	var theFrame = $("#frame-402"),
		prefix = "#" + theFrame.attr("id") + " ",
		date = $(prefix + ".date");
	
	var now = new Date();
	var day, month, year, hours, minutes;
	now.getDay() < 10 ? day = "0" + now.getDay() : day = "" + now.getDay();
	now.getMonth() < 10 ? month = "0" + now.getMonth() : month = "" + now.getMonth();
	year = "" + now.getFullYear();
	hours = "" + now.getHours();
	minutes = "" + now.getMinutes();
	
	date.html(day + "." + month + "." + year + " " + hours + ":" + minutes);
	window.print();
	hideEverythingBut($("#frame-000"));
}

var hideEverythingBut = function(elem)
{
	var frames = $(".frame");
	
	frames.fadeOut(0);
	elem.fadeIn(0);
	
	for (var i = 0; i < audio.length; i++)
		audio[i].pause();	

	for (var i = 0; i < timeout.length; i++)
		clearTimeout(timeout[i]);
	
	switch(elem.attr("id"))
	{
		case "frame-000":
			launch000();
			break;
		case "frame-101":
			launch101();
			break;
		case "frame-102":
			launch102();
			break;
		case "frame-103":
			launch103();
			break;
		case "frame-104":
			launch104();
			break;
		case "frame-105":
			launch105();
			break;
		case "frame-106":
			launch106();
			break;
		case "frame-107":
			launch107();
			break;
		case "frame-107-a":
			launch107a();
			break;
		case "frame-107-b":
			launch107b();
			break;
		case "frame-107-c":
			launch107c();
			break;
		case "frame-108":
			launch108();
			break;
		case "frame-109":
			launch109();
			break;
		case "frame-110":
			launch110();
			break;
		case "frame-111":
			launch111();
			break;
		case "frame-301":
			launch301();
			break;
		case "frame-401":
			launch401();
			break;
		case "frame-402":
			launch402();
			break;
		case "frame-201":
			launch201();
			break;
	}
}

var initMenuButtons = function(){
	var links = $(".link");
	links.click(function(){
		var elem = $("#"+$(this).attr("data-link"));
		hideEverythingBut(elem);
	});
};

var main = function()
{
	initMenuButtons();
	hideEverythingBut($("#frame-000"));
	//loadImages();
};

$(document).ready(main);